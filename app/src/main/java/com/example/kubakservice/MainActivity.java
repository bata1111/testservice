package com.example.kubakservice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startButton = findViewById(R.id.startButton);
        Button stopButton = findViewById(R.id.stopButton);
        TextView numberTextView = findViewById(R.id.numberTextView);
        sharedPreferences = getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);

        numberTextView.setText(sharedPreferences.getString("number", "0"));


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    ComponentName componentName = new ComponentName(MainActivity.this, ServiceHigherO.class);
                    JobInfo info = new JobInfo.Builder(1, componentName)
                            .setRequiresStorageNotLow(true)
                            .setPersisted(true)
                            .setPeriodic(15000 * 60 * 1000)
                            .build();
                    JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
                    jobScheduler.schedule(info);
                } else {
                    Intent intent = new Intent(MainActivity.this, ServiceUnderO.class);
                    startService(intent);
                }
                saveStateInSharedPreferences(true);

            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
                    jobScheduler.cancel(1);
                } else {
                    Intent intent = new Intent(MainActivity.this, ServiceUnderO.class);
                    stopService(intent);
                }
                saveStateInSharedPreferences(false);
            }
        });

    }


    public void saveStateInSharedPreferences(Boolean flag) {
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putBoolean("state", flag);
        prefEditor.apply();
    }

}
