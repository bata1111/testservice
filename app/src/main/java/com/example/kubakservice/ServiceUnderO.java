package com.example.kubakservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;
//import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ServiceUnderO extends Service {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences sharedPreferences = getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);
        boolean serviceActivated = sharedPreferences.getBoolean("state", false);
        if (serviceActivated) {
            counter = Integer.parseInt(sharedPreferences.getString("number", "0"));
        } else {
            counter = 0;
        }

        loop(this);
        return START_STICKY;
        //tells the OS to recreate the service after it has enough memory and call
    }

    @Nullable
    @Override
    public IBinder onBind(@NonNull Intent intent) {
        return null;
    }


    int counter;
    CountDownTimer countDownTimer;
    boolean flag = true;


    private void loop(final Context context) {
        if (flag) {

            countDownTimer = new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    counter++;
//                    Toast.makeText(context, "" + counter, Toast.LENGTH_SHORT).show();
                    saveCounter(context);
                    loop(context);
                }
            }.start();
        }

    }

    public void saveCounter(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putString("number", String.valueOf(counter));
        prefEditor.apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        flag = false;
    }
}
